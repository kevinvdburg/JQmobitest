 var webRoot = "./";
$.ui.autoLaunch = false; //By default, it is set to true and you're app will run right away.  We set it to false to show a splashscreen
/* This function runs when the body is loaded.*/
var init = function () {
        $.ui.backButtonText = "Back";// We override the back button text to always say "Back"
        window.setTimeout(function () {
            $.ui.launch();
        }, 1500);//We wait 1.5 seconds to call $.ui.launch after DOMContentLoaded fires
    };
document.addEventListener("DOMContentLoaded", init, false);

$.ui.ready(function () {

	document.addEventListener("menubutton", menuSlide, false);
	
	function menuSlide(){
		$.ui.toggleSideMenu();
	}
});


// Called when capture operation is finished
//
function captureSuccess(mediaFiles) {
    var i, len;
    for (i = 0, len = mediaFiles.length; i < len; i += 1) {
        uploadFile(mediaFiles[i]);
    }       
}

// Called if something bad happens.
// 
function captureError(error) {
    var msg = 'An error occurred during capture: ' + error.code;
    navigator.notification.alert(msg, null, 'Uh oh!');
}

// A button will call this function
//
function captureVideo() {
    // Launch device video recording application, 
    // allowing user to capture up to 2 video clips
    navigator.device.capture.captureVideo(captureSuccess, captureError, {limit: 2});
}

// Upload files to server
function uploadFile(mediaFile) {
    var ft = new FileTransfer(),
        path = mediaFile.fullPath,
        name = mediaFile.name;

    ft.upload(path,
        "http://my.domain.com/upload.php",
        function(result) {
            console.log('Upload success: ' + result.responseCode);
            console.log(result.bytesSent + ' bytes sent');
        },
        function(error) {
            console.log('Error uploading file ' + path + ': ' + error.code);
        },
        { fileName: name });   
}

         
            
 
            /* This code is used for native apps */
var onDeviceReady = function () {
        AppMobi.device.setRotateOrientation("portrait");
        AppMobi.device.setAutoRotate(false);
        webRoot = AppMobi.webRoot + "/";
        //hide splash screen
        AppMobi.device.hideSplashScreen();	
        alert('hoi!');
   
};
document.addEventListener("appMobi.device.ready", onDeviceReady, false);